package fyber.challenge

import java.io.InputStream
import java.text.DecimalFormat

import scala.concurrent.duration.{Duration, _}
import scala.io.Source

/**
  * Time Series challenge.
  *
  * Assumptions:
  * - By `small program` you mean less than 100 lines
  * - Not all tests are expected, but most important ones (i.e: TimeSeriesRenderer is not unit-tested)
  * - No Facade is expected that abstracts the solution itself. Components are wired on the `main` app.
  * - Input Data file is available on classpath, not expected to be found on File System.
  * -
  */
object TimeSeriesApp extends App {

  val file = args.headOption.getOrElse("/data_scala.txt")

  val parser = new TimeSeriesParser()
  val render = new TimeSeriesRenderer()
  val timeSeries = new TimeSeries()

  val dataStream = parser.parse(getClass.getResourceAsStream(file))
  val result = timeSeries.process(dataStream, 60.seconds)

  render.render(result)

}

class TimeSeriesParser {

  def parse(inputStream: InputStream): Stream[TimeSeriesInput] = {
    val lines = Source.fromInputStream(inputStream).getLines().toStream
    lines.map { line =>
      val Array(timeString, valueString) = line.split("\\t")
      TimeSeriesInput(timeString.toLong, valueString.toDouble)
    }
  }

}

case class TimeSeriesInput(time: Long, value: Double)

case class TimeSeriesOutput(time: Long,
                            value: Double,
                            observations: Long,
                            rollingSum: Double,
                            minValue: Double,
                            maxValue: Double)

class TimeSeries {

  object Accumulator {
    def first(value: Double) = Accumulator(1L, value, value, value)
  }

  case class Accumulator(observations: Long,
                         rollingSum: Double,
                         minValue: Double,
                         maxValue: Double) {

    def take(value: Double): Accumulator = {
      copy(observations + 1, rollingSum + value, math.min(value, minValue), math.max(value, maxValue))
    }
  }

  def process(dataStream: Stream[TimeSeriesInput], window: Duration): Stream[TimeSeriesOutput] = {

    def newOutput(data: TimeSeriesInput, accum: Accumulator): TimeSeriesOutput =
      TimeSeriesOutput(data.time, data.value, accum.observations, accum.rollingSum, accum.minValue, accum.maxValue)

    def slice(start: Long, stream: Stream[TimeSeriesInput]): Stream[TimeSeriesInput] = {
      val end = start + window.toSeconds
      stream.takeWhile(_.time <= end)
    }

    def processSlice(stream: Stream[TimeSeriesInput], maybeAccumulator: Option[Accumulator]): Stream[TimeSeriesOutput] = {
      stream.headOption.map { data =>
        val newAccum = maybeAccumulator.map(_.take(data.value)).getOrElse(Accumulator.first(data.value))
        newOutput(data, newAccum) #:: processSlice(stream.tail, Some(newAccum))
      }.getOrElse(Stream.empty)
    }

    def map(stream: Stream[TimeSeriesInput]): Stream[Stream[TimeSeriesOutput]] = {
      stream.headOption.map { data =>
        processSlice(slice(data.time, stream), None) #:: map(stream.tail)
      }.getOrElse {
        Stream.empty
      }
    }

    def reduce(slices: Stream[Stream[TimeSeriesOutput]]): Stream[TimeSeriesOutput] = {
      def reduceFrom(from: Long, slices: Stream[Stream[TimeSeriesOutput]]): Stream[TimeSeriesOutput] = {
        slices.headOption.map { slice =>
          val rest = slice.dropWhile(_.time <= from)
          val nextFrom = slice.maxBy(_.time).time
          rest #::: reduceFrom(nextFrom, slices.tail)
        }.getOrElse(Stream.empty)
      }

      reduceFrom(0L, slices)
    }

    (map _ andThen reduce)(dataStream)

  }

}

class TimeSeriesRenderer {

  private val formatter = new DecimalFormat("#0.00####")

  def render(data: Stream[TimeSeriesOutput], length: Option[Int] = None, out: Appendable = System.out): Unit = {

    def format(d: Double) = formatter.format(d)

    out.append("     T        V    N  RS     MinV    MaxV\n--------------------------------------------------\n")
    length.fold(data)(data.take).foreach { d =>
      out.append("%10d %-7s %d %-7s %-7s %-7s\n".format(
        d.time,
        format(d.value),
        d.observations,
        format(d.rollingSum),
        format(d.minValue),
        format(d.maxValue)))
    }
  }

}

package fyber.challenge

import java.io.StringWriter

import org.scalatest.FlatSpec

import scala.concurrent.duration._

class FunctionalSpec extends FlatSpec {

  val parser = new TimeSeriesParser

  val stream = parser.parse(getClass.getResourceAsStream("/data_scala.txt"))

  lazy val expectedOutput =
   """|     T        V    N  RS     MinV    MaxV
      |--------------------------------------------------
      |1355270609 1.80215 1 1.80215 1.80215 1.80215
      |1355270621 1.80185 2 3.604   1.80185 1.80215
      |1355270646 1.80195 3 5.40595 1.80185 1.80215
      |1355270702 1.80225 2 3.6042  1.80195 1.80225
      |1355270702 1.80215 3 5.40635 1.80195 1.80225
      |1355270829 1.80235 1 1.80235 1.80235 1.80235
      |1355270854 1.80205 2 3.6044  1.80205 1.80235
      |1355270868 1.80225 3 5.40665 1.80205 1.80235
      |1355271000 1.80245 1 1.80245 1.80245 1.80245
      |1355271023 1.80285 2 3.6053  1.80245 1.80285
      |1355271024 1.80275 3 5.40805 1.80245 1.80285
      |1355271026 1.80285 4 7.2109  1.80245 1.80285
      |1355271027 1.80265 5 9.01355 1.80245 1.80285
      |1355271056 1.80275 6 10.8163 1.80245 1.80285
      |1355271428 1.80265 1 1.80265 1.80265 1.80265
      |1355271466 1.80275 2 3.6054  1.80265 1.80275
      |1355271471 1.80295 3 5.40835 1.80265 1.80295
      |1355271507 1.80265 3 5.40835 1.80265 1.80295
      |1355271562 1.80275 2 3.6054  1.80265 1.80275
      |1355271588 1.80295 2 3.6057  1.80275 1.80295
      |""".stripMargin

  implicit def asTimeSeriesInput(t: (Long, Double)): TimeSeriesInput = TimeSeriesInput(t._1, t._2)

  "TimeSeriesParser" should "parse all lines in file" in {
    assert( stream.length === 9316 )
  }

  it should "parse first line" in {
    assert( stream.head === TimeSeriesInput(1355270609, 1.80215) )
  }

  it should "parse last line" in {
    assert( stream.last === TimeSeriesInput(1355522474, 1.77655) )
  }

  it should "parse keeping asc order by time" in {
    assert( stream.head.time <= stream.last.time )
  }

  "TimeSeries" should "process all lines" in {
    val ts = new TimeSeries()
    val input = List[TimeSeriesInput](1L -> 1.1, 2L -> 2.2, 20L -> 2.3, 30L -> 2.2, 400L -> 4.4, 404L -> 5.5).toStream
    val result = ts.process(input, 60.seconds).toList
    assert( result.head === TimeSeriesOutput(1L, 1.1, 1, 1.1, 1.1, 1.1) )
  }

  it should "consider empty input" in {
    val ts = new TimeSeries()
    val input = List[TimeSeriesInput]().toStream
    val result = ts.process(input, 60.seconds).toList
    assert( result.isEmpty )
  }

  it should "process challenge input producing expected output" in {

    val parser1 = new TimeSeriesParser()

    val inputs = parser1.parse(getClass.getResourceAsStream("/data_scala.txt"))
    val ts = new TimeSeries()
    val result = ts.process(inputs, 60.seconds)
    result.size === inputs.size

    val render = new TimeSeriesRenderer()
    val writer = new StringWriter()
    render.render(result, Some(20), writer)

    val resultString = writer.toString
    assert( resultString === expectedOutput )
  }

}
